#include <iostream>
#include <array>
#include <random>

int main(void)
{
	std::array<std::string, 9> games_list = 
	{
		"Breakout",
		"Pong",
		"Flappy Bird",
		"Asteroids",
		"Galaga",
		"Rock, Paper, Scissors",
		"Guess the Number",
		"Hi-Lo",
		"FizzBuzz"
	};
	
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> distribution(0, games_list.size() - 1);

	std::cout << games_list[distribution(rng)] << "\n";
	return 0;
}
