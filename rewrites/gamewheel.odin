package GameWheel

import "core:math/rand"
import "core:fmt"

main :: proc()
{
	games_list: []string =
	{
		"Breakout",
		"Pong",
		"Flappy Bird",
		"Asteroids",
		"Galaga",
		"Rock, Paper, Scissors",
		"Guess the Number",
		"Hi-Lo",
		"FizzBuzz"
	}

	fmt.println(rand.choice(games_list))
}