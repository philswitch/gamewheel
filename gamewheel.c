#!/usr/bin/tcc -run 
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#define arr_count(x) (sizeof(x)/sizeof(x)[0])

int main(void)
{
	char* games_list[] = 
	{
		"Breakout",
		"Pong",
		"Flappy Bird",
		"Asteroids",
		"Galaga",
		"Rock, Paper, Scissors",
		"Guess the Number",
		"Hi-Lo",
		"FizzBuzz"
	};

	srandom(time(NULL));
	printf("%s\n", games_list[random() % arr_count(games_list)]);
	return 0;
}
